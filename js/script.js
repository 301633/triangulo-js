$(document).ready(function(){
    
    $(document).on("click", "button", function(){
        
        var lados = [];
        
        lados.push(parseFloat($("input.a").val()));
        lados.push(parseFloat($("input.b").val()));
        lados.push(parseFloat($("input.c").val()));
        
        lados.sort(function(a, b){
            return a - b;
        });
        
        console.log(lados);
        if(lados[0] + lados[1] > lados[2]){
            
            if(lados[0] == lados[1]){
                if(lados[0] == lados[2]){
                    alert("Triangulo Equilátero");
                }else{
                    alert("Triangulo Isósceles");
                }
            }else if(lados[1] == lados[2]){
                    alert("Triangulo Isósceles"); 
            }else if(lados[0] == lados[2]){
                    alert("Triangulo Isósceles");
            }else{
                alert("Triangulo Escaleno");
            }
            
        }else{
            alert("Error en los datos! :(");
        }
        
        $("input").val("");
        $("input.a").focus();
    });
    
});